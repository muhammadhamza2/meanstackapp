var mongoose = require('mongoose');

// Create the MovieSchema.
var ProfileSchema = new mongoose.Schema({
  task: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  pass: {
    type: String,
    required: true
  },
  done: {
    type: String,
    required: true
  }
 ,
  token: {
    type: String,
    required: false
  }
},{ collection: 'Data'});

// Export the model schema.
module.exports = ProfileSchema;
