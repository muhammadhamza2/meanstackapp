var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var _ = require('lodash');
//var jwt = require('express-jwt');

// Create the application.
var app = express();
/*
var jwtCheck = jwt({
  secret: new Buffer('YOUR_CLIENT_SECRET', 'base64'),
  audience: 'YOUR_CLIENT_ID'
});*/

// Add Middleware necessary for REST API's
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));

// CORS Support
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:63342');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', 'true');

  next();
});

//app.use('/Profile', jwtCheck);

// Connect to MongoDB
mongoose.connect('mongodb://localhost/LOgin');
mongoose.connection.once('open', function() {




  // Load the models.
  app.models = require('./models/index'); //=> 'key' Profile: 'value' ProfileSchema and ProfileSchema Contains Collection name and model of collection

  // Load the routes.
  var routes = require('./routes'); //=> 'key' /Profile :
  _.each(routes, function(controller, route) {
     // console.log(route+"its controller is " +controller)
    app.use(route, controller(app, route));
  });



  console.log('Listening on port 3000...');
  app.listen(3000);
});
