/**
 * Created by hamza on 7/8/15.
 */
routerApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/root/home');

    // .when('/c?id', '/contacts/:id')
    //.when('/user/:id', '/contacts/:id')

    $stateProvider

        .state('root', {
            url: '/root',
            templateUrl: 'src/Partials/root.html',
            controller:'NavController'
        })


        .state('home', {
            url: '/home',
            templateUrl: 'src/Partials/HomePartial.html',
            parent: 'root',
            controller:'HomeController'
        })

        .state('LogOut', {
            url: '/LogIn',
            templateUrl: 'src/Partials/LoginPartial.html',
            parent: 'root',
            controller:'LoginController'
        })

        .state('AddEmp', {
            url: '/AddEmp',
            templateUrl: 'src/Partials/empAddPartial.html',
            controller:'routerAppController',
            parent: 'root'

        })

        .state('AddEmp.details', {
            url: '/details/:eID',
            templateUrl: 'src/Partials/detials.html',
            controller:'routerAppController',
            parent: 'root' // default was root

        });

});